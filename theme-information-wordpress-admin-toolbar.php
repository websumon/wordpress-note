<?php

function theme_information_in_wordpress_admin_toolber( $wp_admin_bar ) {
	
	$current_theme = wp_get_theme();
	$theme_name = $current_theme->get( 'Name' );
	$theme_version = $current_theme->get( 'Version' );
		
	$args = array(
		'id'    => 'current_theme',
		'title' => esc_html( $theme_name ) . " " . esc_html( $theme_version ),
		'meta'  => array( 'class' => 'current-theme' )
	);
	
	$wp_admin_bar->add_node( $args );
}
add_action( 'admin_bar_menu', 'theme_information_in_wordpress_admin_toolber', 999 );